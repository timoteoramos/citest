import express from "express";
const app = express();

app.get('/', (req, res) => {
  res.send('<p>It works!</p>');
});

app.listen(3000);

export default app;
