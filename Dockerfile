FROM alpine:latest

EXPOSE 3000

WORKDIR /srv

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]

CMD [ "/usr/bin/node", "/srv/index.js" ]

RUN apk add dumb-init nodejs npm && adduser -D -h /srv srv && chown srv:srv /srv

COPY --chown=srv:srv ./index.js ./package-lock.json ./package.json /srv/

USER srv

RUN npm install
